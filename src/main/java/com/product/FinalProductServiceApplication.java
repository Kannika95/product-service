package com.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class FinalProductServiceApplication extends ResponseFile{
	

	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}

	@Bean
	public WebClient webClient(){
		return WebClient.builder().build();
	}
	public static void main(String[] args) {
		ResponseFile.a=20;
		System.out.println("static......"+ResponseFile.a);
//		try{
//
//			  System.out.println("try........");
//			  new RuntimeException("exception Message");
//			}
//			catch(Exception ex) {
//System.out.println("catch...........");
//			        ex.printStackTrace();
//
//			}
		SpringApplication.run(FinalProductServiceApplication.class, args);
	}

}
